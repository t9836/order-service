const winston = require('winston')
require('winston-logstash');


winston.add(winston.transports.Logstash,
{
    port: 10113,
    host: '343e0cf4-eb64-4642-8aed-f8a94ed80b47-ls.logit.io',
    ssl_enable: true,
    max_connect_retries: -1,
});

module.exports = {
    logger: winston
}
