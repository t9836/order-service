const path = require('path');
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const PROTO_PATH = path.join(__dirname, '../proto/order.proto');
const {
    sellerGetAllOrder,
    sellerPatchOrder
} = require('../controller/sellerController');
const {
    buyerGetHistoryOrder,
    buyerCreateOrderBuyer,
    buyerGetOrderDetail,
    buyerSubmitReviewOrder
} = require('../controller/buyerController');

const orderPackageDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enum: String,
    arrays: true
})

const orderProto = grpc.loadPackageDefinition(orderPackageDefinition);
const server = new grpc.Server();


server.addService(orderProto.OrderService.service, {
    // Define all proto function in Order Service
    sellerGetAllOrder: async (call, callback) => {
        const orderData = await sellerGetAllOrder(call.request.username_seller, call.request.authorization)
        callback(null, {orders: orderData})
    },

    sellerPatchOrder: async (call, callback) => {
        const sellerPatchSucceed = await sellerPatchOrder(
            call.request.id_order,
            call.request.status
        )

        if (sellerPatchSucceed) {
            callback(null, {status_code: 200, message:"Order updated."})
        } else {
            callback(null, {status_code: 400, message: 'Failed'})
        }
    },

    buyerGetAllOrder: async (call, callback) => {
        const orderData = await buyerGetHistoryOrder(call.request.username_buyer, call.request.authorization);
        callback(null, {orders: orderData})
    },

    buyerGetOrderDetail: async (call, callback) => {
        const orderDetailData = await buyerGetOrderDetail(call.request.order_id);
        callback(null, { order: orderDetailData })
    },

    buyerPostCreateOrder: async (call, callback) => {
        const buyerPostSucceed = await buyerCreateOrderBuyer(
            call.request.username_buyer,
            call.request.username_seller,
            call.request.id_food,
            call.request.quantity
        )

        if (buyerPostSucceed) {
            callback(null, {status_code: 200, message: "Order Created."})
        } else {
            callback(null, {status_code: 400, message: 'Failed'})
        }
    },

    buyerPostReviewOrder: async (call, callback) => {
        const buyerPostReviewSucceed = await buyerSubmitReviewOrder(call.request.order_id)

        if (buyerPostReviewSucceed) {
            callback(null, {status_code: 200, message: "Review submitted."})
        } else {
            callback(null, {status_code: 400, message: 'Failed'})
        }
    }

})

server.bind(`localhost:${process.env.GRPC_SERVER_PORT || 4040}`, grpc.ServerCredentials.createInsecure())
server.start()
console.log(`Server running at localhost:${process.env.GRPC_SERVER_PORT || 4040}`)