const path = require('path');
const grpc = require('grpc');
const protoLoader = require("@grpc/proto-loader")
const PROTO_PATH = path.join(__dirname, '../proto/order.proto');

const orderPackageDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    arrays: true
});

const OrderService = grpc.loadPackageDefinition(orderPackageDefinition).OrderService;
const client = new OrderService(
    `localhost:${process.env.GRPC_SERVER_PORT || 4040}`,
    grpc.credentials.createInsecure()
)

// Example Request
// client.sellerGetAllOrder({usernameSeller: "ddelvo"}, (err, res) => {
//     if (!err) {
//         console.log(res);
//     } else {
//         console.log("Error:", err.message);
//     }
// })

// client.sellerPatchOrder({idOrder: 1, status: 1}, (err, res) => {
//     if (!err) {
//         console.log(res);
//     } else {
//         console.log("Error:", err.message);
//     }
// })

module.exports = client;