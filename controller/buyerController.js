const axios = require('axios');
const { Order } = require('../database/models/Order')

const buyerGetHistoryOrder = async (username_buyer, authorization) => {
    // Fetch to food service
    const options = {
        headers: {
            authorization
        }
    }

    let res;
    try {
        res = await axios.get('https://kuymakan-food.herokuapp.com/food/', options)
    } catch (err) {
        return err;
    }

    const foodData = res.data
    let foodDataObj = {}
    for (let i = 0; i < foodData.length; i++) {
        const idFood = foodData[i].id
        foodDataObj[idFood] = foodData[i]
    }

    let allOrderData = await Order.findAll(
        {
            where: {
                username_buyer: username_buyer
            }
        }
    );

    let result = []
    for (let i = 0; i < allOrderData.length; i++) {
        try {
            const data = foodDataObj[allOrderData[i].id_food]
            result.push({
                id:allOrderData[i].id,
                id_food:allOrderData[i].id_food,
                quantity:allOrderData[i].quantity,
                status:allOrderData[i].status,
                date:allOrderData[i].date,
                username_buyer:allOrderData[i].username_buyer,
                username_seller:allOrderData[i].username_seller,
                name: data.nama,
                price: data.harga,
                is_reviewed: allOrderData[i].is_reviewed
            })
        } catch (err) {
            continue
        }
    }

    return result;

}

const buyerCreateOrderBuyer = async (username_buyer, username_seller, id_food, quantity) => {
    try {
        const orderData = {
            id_food: id_food,
            username_buyer: username_buyer,
            username_seller: username_seller,
            date: new Date(),
            quantity: quantity,
            status: 0,
            is_reviewed: false
        }

        const createdOrder = await Order.create(orderData);
        return true;

    } catch (err) {
        console.log(err)
        return false;
    }
}

const buyerGetOrderDetail = async (order_id) => {
    try {
        const orderByOrderId = await Order.findByPk(order_id)
        return orderByOrderId
    } catch(err){
        console.log(err)
        return err
    }
}

const buyerSubmitReviewOrder = async (order_id) => {
    try {
        const updatedOrder = await Order.update(
            {
                is_reviewed: true
            }, {
                where: {
                    id: order_id
                }
            }
        )

        return true;
    } catch (err) {
        return false;
    }
}

module.exports = {
    buyerCreateOrderBuyer,
    buyerGetHistoryOrder,
    buyerGetOrderDetail,
    buyerSubmitReviewOrder
}