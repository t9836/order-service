const axios = require('axios');
const { Order } = require('../database/models/Order')


const sellerGetAllOrder = async (username_seller, authorization) => {
    // Fetch to food service
    const options = {
        headers: {
            authorization
        }
    }
    
    let res;
    try {
        res = await axios.get('https://kuymakan-food.herokuapp.com/food/', options);
    } catch (err) {
        return err
    }


    const foodData = res.data
    let foodDataObj = {}
    for (let i = 0; i < foodData.length; i++) {
        const idFood = foodData[i].id
        foodDataObj[idFood] = foodData[i]
    }

    let allOrderData = await Order.findAll(
        {
            where: {
                username_seller: username_seller
            }
        }
    );


    let result = []
    for (let i = 0; i < allOrderData.length; i++) {
        try {
            const data = foodDataObj[`${allOrderData[i].id_food}`]
            result.push({
                id:allOrderData[i].id,
                id_food:allOrderData[i].id_food,
                quantity:allOrderData[i].quantity,
                status:allOrderData[i].status,
                date:allOrderData[i].date,
                username_buyer:allOrderData[i].username_buyer,
                username_seller:allOrderData[i].username_seller,
                name: data.nama,
                price: data.harga,
                image: data.foto,
                is_reviewed: allOrderData[i].is_reviewed
            })
        } catch (err) {
            continue
        }
    }

    return result;
}

const sellerPatchOrder = async (id_order, status) => {
    try {
        const updatedOrder = await Order.update(
        {
            status: status
        },
            {
                where: {
                    id: id_order
                }
            }
        )
        return true;
    } catch (err) {
        return false;
    }
}

module.exports = {
    sellerGetAllOrder,
    sellerPatchOrder
}