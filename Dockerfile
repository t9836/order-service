FROM node:14

# Create app directory
WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./
COPY proto/ ./
COPY grpc/ ./
COPY rest/ ./
COPY controller/ ./
COPY database/ ./
COPY nginx/ ./

RUN npm install sequelize
RUN npm install
RUN npm rebuild

# Bundle app source
COPY . .
