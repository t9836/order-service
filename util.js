const sequelize = require('sequelize');
const Order = require('./database/models/Order')

const dropTable = async () => {
    await Order.drop();
    console.log("Order table dropped");
}

dropTable();