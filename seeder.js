const { db } = require('./database');
const { Order } = require('./database/models/Order');

const seedOrder = async () => {
    try {
        await db.sync({force: false})
    } catch (err) {
        console.log('Error in syncing')
        return;
    }

    for (let i = 0; i < 5; i++) {
        const orderData = {
            id_food: i % 3,
            username_buyer: 'ddelvo',
            username_seller: 'fikri',
            date: new Date(),
            quantity: i,
            status: i % 4,
        }
        try {
            const createdData = await Order.create(orderData);
        } catch (err) {
            console.log('error')
        }
    }

    console.log('Order data seeded.')
}


const start = async () => {
    try {
        seedOrder()
    } catch (err) {
        console.log("Error in seeding")
    }
}

start()