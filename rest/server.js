const express = require('express');
const bodyParser = require('body-parser');
const grpcClient = require('../grpc/grpc-client.js');
const { db } = require('../database')
const axios = require('axios');
const {logger} = require('../log')

const app = express();
const cors = require('cors');
const { request } = require('express');

// Middleware 
app.use(bodyParser.json())
app.use(cors())

// Service restriction (need bearer)
app.use(async (req, res, next) => {
    if (!req.headers.authorization) {
        logger.error(`[${req.method}] Request from ${req.headers.host} doesn't have bearer token`)
        return res.status(403).json({
            "status_code":403,
            "message": "No credentials!"
        })
    } else {
        const token = req.headers.authorization
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': token
        }
        try {
            const checkToken = await axios.post('https://oauth-a7law.herokuapp.com/oauth/resource', {}, {
                headers: headers
            })

            next()
        } catch (err) {
            logger.error(`[${req.method}] Request from ${req.headers.host} bearer token invalid`)
            return res.status(403).json({
                "status_code":403,
                "message": "Credentials not valid!"
            })
        }
    }
})

// Seller
app.get('/order/seller', (req, res) => {
        grpcClient.sellerGetAllOrder({username_seller: req.query.username_seller, authorization: req.headers.authorization}, (error, response) => {
            if (!error) {
                logger.info(`[GET] 'order/seller' Request from ${req.headers.host}`)
                return res.status(200).json({
                    "status_code": 200,
                    "message": "OK",
                    "data": response.orders,
                })
            } else {
                console.log(error)
                logger.error(`[GET] 'order/seller' Request from ${req.headers.host}, Error message: ${error}`)
                return res.status(400).json({
                    'status_code': 400,
                    "error": "invalid_request",
                    "error_description": "Something went error in server"
                })
            }
    })
})

app.patch('/order/seller', (req, res) => {
    grpcClient.sellerPatchOrder({
        id_order: req.body.id_order,
        status: req.body.status
    }, (error, response) => {
        if (response.status_code === 200) {
            logger.info(`[PATCH] 'order/seller' Request from ${req.headers.host}`)
            return res.status(200).json({
                "status_code": 200,
                "message": "Order updated."
            })
        } else {
            
            logger.error(`[GET] 'order/seller' Request from ${req.headers.host}, Error message: ${error}`)
            return res.status(400).json({
                'status_code': 400,
                "error": "invalid_request",
                "error_description": "Something went error in server"
            })
        }
    })
})


// Buyer
app.get('/order/buyer/history', (req, res) => {
        grpcClient.buyerGetAllOrder({username_buyer: req.query.username_buyer, authorization: req.headers.authorization}, (error, response) => {
        if (!error) {
            logger.info(`[GET] 'order/buyer/history' Request from ${req.headers.host}`)
            return res.status(200).json({
                "status_code": 200,
                "message": "OK",
                "data": response.orders,
            })
        } else {
            console.log(error)
            logger.error(`[GET] 'order/buyer/history' Request from ${req.headers.host}, Error message: ${error}`)
            return res.status(400).json({
                'status_code': 400,
                "error": "invalid_request",
                "error_description": "Something went error in server"
            })
        }
    })
})

app.get('/order/buyer/order-detail/', (req, res) => {
    grpcClient.buyerGetOrderDetail({ order_id: req.query.order_id}, (error, response) => {
        if (!error){
            logger.info(`[GET] 'order/buyer/order-detail' Request from ${req.headers.host}`)
            if (response.order === undefined){
                return res.status(400).json({
                    "status_code": 400,
                    "error": "not found",
                    "error_description": `order with order_id of ${req.query.order_id} is not found`
                })
            }
            return res.status(200).json({
                "status_code": 200,
                "message": "OK",
                "data": response.order
            })
        } else {
            console.log(error)
            logger.error(`[GET] 'order/buyer/order-detail' Request from ${req.headers.host}, Error message: ${error}`)
            return res.status(400).json({
                "status_code": 400,
                "error": "invalid_request",
                "error_description": "Something went wrong in the server"
            })
        }
    })
})

app.post('/order/buyer', (req, res) => {
    grpcClient.buyerPostCreateOrder({
        username_buyer: req.body.username_buyer,
        username_seller: req.body.username_seller,
        id_food: req.body.id_food,
        quantity: req.body.quantity
    }, (error, response) => {
        if (response.status_code === 200) {
            logger.info(`[POST] 'order/buyer' Request from ${req.headers.host}`)
            return res.status(200).json(response)
        } else {
            logger.error(`[POST] 'order/buyer' Request from ${req.headers.host}, Error message: ${error}`)
            return res.status(400).json({
                'status_code': 400,
                "error": "invalid_request",
                "error_description": "Something went error in server"
            })
        }
    })
})


app.post('/order/buyer/review', (req, res) => {
    grpcClient.buyerPostReviewOrder({
        order_id: req.body.order_id
    }, (err, response) => {
        if (response.status_code === 200) {
            logger.info(`[POST] 'order/buyer/review' Request from ${req.headers.host}`)
            return res.status(200).json(response)
        } else {
            logger.error(`[POST] 'order/buyer/review' Request from ${req.headers.host}, Error message: ${error}`)
            return res.status(400).json({
                'status_code': 400,
                "error": "invalid_request",
                "error_description": "Something went error in server"
            })
        }
    })
})


const PORT = process.env.REST_SERVER_PORT || 5050;
const start = async () => {
    try {
        await db.sync({
            // force: false,
            alter: true
        })

        app.listen(PORT, () => {
            console.log("Server running at port %d", PORT);
        });

    } catch (err) {
        console.log(err)
    }
}

start()