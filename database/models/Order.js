const Sequelize = require('sequelize');
const { db } = require('../index');

const Order = db.define('Order', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    id_food: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    username_buyer: {
        type: Sequelize.STRING,
        allowNull: false
    },
    username_seller: {
        type: Sequelize.STRING,
        allowNull: false
    },
    date: {
        type: Sequelize.DATE,
        allowNull: false
    },
    quantity: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    status: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    is_reviewed: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
    }
}, {tableName: "Order"})

module.exports = {
    Order
}